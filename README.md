RTL8821AE wifi drivers in Ubuntu 16.04

First thing first, you need to identify which device is being used in your hardware, to find it, I opened a terminal and wrote the following:

    lspci | grep Wireless

The system replies with the following line

    03:00.0 Network controller: Realtek Semiconductor Co., Ltd. RTL8821AE 802.11ac PCIe Wireless Network Adapter

The model of the device (RTL8821AE) is very important, we will come back to this later.

For some reason, realtek adapters have had a history of disconnects and signal drops, but, lucky for us there is an answer.

    sudo apt-get install linux-headers-generic build-essential git 
    git clone http://github.com/lwfinger/rtlwifi_new.git
    cd rtlwifi_new
    make
    sudo make install

These commands build and install the drivers for
rtl8192ce, rtl8192se, rtl8192de, rtl8188ee, rtl8192ee, rtl8723ae, rtl8723be, and rtl8821ae, all in one go.

Just in case the system doesn�t load the appropriate kernel module, you can execute the following command from within your rtlwifi_new directory

    sudo modprobe rtl8821ae

and reboot your system.

Congratulations your Ubuntu Linux kernel now has working Realtek wireless drivers.


Opening ports

    sudo iptables -I INPUT -p tcp --dport 5000 -j ACCEPT
    sudo iptables -I OUTPUT -p tcp --dport 5000 -j ACCEPT
    sudo iptables -I FORWARD -p tcp --dport 5000 -j ACCEPT


remove problem detected

    sudo rm /var/crash/*


MS Office install on Ubuntu 17.10

    sudo apt-get install winbind
    rm ~/.PlayOnLinux/wine/linux*/*/lib*/libz*
