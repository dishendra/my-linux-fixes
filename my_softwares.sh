sudo apt update
sudo apt install curl git python3-pip
sudo apt-get install -y build-essential
sudo apt-get install apt-transport-https


# Google Chrome

#Setup key with:
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add - 
#Setup repository with:

sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

# Papirus
sudo add-apt-repository ppa:papirus/papirus

# Nodejs 8
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

# Sublime Text 3
wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list


sudo apt-get update
sudo apt-get install google-chrome-stable
sudo apt-get install papirus-icon-theme
sudo pip3 install ipython ptpython
sudo apt-get install -y nodejs
sudo apt-get install sublime-text


